-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: e-question
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Multiple Choice'),(2,'Incomplete'),(3,'Conversation');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `choice`
--

DROP TABLE IF EXISTS `choice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `choice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `choice_content` varchar(45) DEFAULT NULL,
  `question_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_choice_question_idx` (`question_id`),
  CONSTRAINT `fk_choice_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `choice`
--

LOCK TABLES `choice` WRITE;
/*!40000 ALTER TABLE `choice` DISABLE KEYS */;
INSERT INTO `choice` VALUES (1,'to live',1),(2,'to have lived',1),(3,'to be lived',1),(4,'to be living',1),(5,'on account of',2),(6,'due',2),(7,'because',2),(8,'owing',2),(9,'not having said',3),(10,'have never said',3),(11,'never said',3),(12,'had never said',3),(13,'to be abducted',4),(14,'to be abducting',4),(15,'to have been abducted',4),(16,'to have been abducting',4),(17,'herself',5),(18,'her',5),(19,'her own',5),(20,'hers',5),(21,'Not wanting',6),(22,'As not wanting',6),(23,'She\'s didn\'t want',6),(24,'Because not wanting',6),(25,'There\'s no point',7),(26,'It\'s no point',7),(27,'There isn\'t point',7),(28,'It\'s no need',7),(29,'had written',8),(30,'has written',8),(31,'had been writing',8),(32,'wrote',8),(33,'had',9),(34,'did',9),(35,'got',9),(36,'were',9),(37,'In no way was he',10),(38,'No way he was',10),(39,'In any way he was',10),(40,'In any way was he',10),(41,'may not have been',11),(42,'may not be',11),(43,'might not be',11),(44,'must not have been',11),(45,'were made sleeping',12),(46,'were made sleep',12),(47,'were made to sleep',12),(48,'made to sleep',12),(49,'if he sent',13),(50,'had he sent',13),(51,'if he has sent',13),(52,'did he sent',13),(53,'is to be achieved',14),(54,'is achieved',14),(55,'will be achieved',14),(56,'is due to achieve',14),(57,'It\'s not allowed offering',15),(58,'It\'s not permitted to offer',15),(59,'It\'s not permitted offering',15),(60,'It\'s not allowed to offer',15),(61,'Dolphins',16),(62,'Chimpanzees',16),(63,'Big apes',16),(64,'Mammals',16),(65,'Fish',17),(66,'Men',17),(67,'Animals',17),(68,'Reptiles',17),(69,'Men can now talk to them.',18),(70,'They can teach men their languages.',18),(71,'They can speak to one another. ',18),(72,'They understand simple language.',18),(73,'Men want to talk to dolphins in secret.',19),(74,'Dolphins cannot hear men speaking. ',19),(75,'Most men do not speak English.',19),(76,'The dolphins language is hard to learn.',19),(77,'They often jump on to ships.',20),(78,'They often follow ships. ',20),(79,'They like interesting things about man.',20),(80,'They seem to like stories.',20);
/*!40000 ALTER TABLE `choice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paragraph`
--

DROP TABLE IF EXISTS `paragraph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paragraph` (
  `id` int NOT NULL AUTO_INCREMENT,
  `para_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `category_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_paragraph_category_idx` (`category_id`),
  CONSTRAINT `fk_paragraph_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paragraph`
--

LOCK TABLES `paragraph` WRITE;
/*!40000 ALTER TABLE `paragraph` DISABLE KEYS */;
INSERT INTO `paragraph` VALUES (1,'For many years people believed that the cleverest animals after man were chimpanzees. Now, however, there is proof that dolphins may be even cleverer than these big apes. \nDolphins have a simple language. They are able to talk to one another. It may be possible for man to learn how to talk to dolphins. But this will not be easy because dolphins can not hear the kind of sounds man can make. If man wants to talk to dolphins, therefore, he will have to make a third language which both he and the dolphins can understand.\nDolphins are also very friendly towards man. They often follow ships. There are many stories of dolphins guiding ships through difficult and dangerous waters.',3),(2,'A friendly letter \nDear Sally, \nI\'m really looking forward (1)________ you again. I can\'t wait for summer (2)________. Once the school finishes, I\'ll write to you again so that we can arrange (3)________ we\'re going to go holiday. I\'m going (4)________ to my parents and persuade them to let me (5)________ a bit longer with you this time.',2);
/*!40000 ALTER TABLE `paragraph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question_content` text,
  `category_id` int DEFAULT NULL,
  `para_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_question_category_idx` (`category_id`),
  KEY `fk_question_paragraph_idx` (`para_id`),
  CONSTRAINT `fk_question_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `fk_question_paragraph` FOREIGN KEY (`para_id`) REFERENCES `paragraph` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'I\'m very happy _____ in India. I really miss being there.',1,NULL),(2,'They didn\'t reach an agreement ______ their differences.',1,NULL),(3,'I wish I _____ those words. But now it\'s too late.',1,NULL),(4,'The woman, who has been missing for 10 days, is believed _____.',1,NULL),(5,'She was working on her computer with her baby next to _____.',1,NULL),(6,'_____ to offend anyone, she said both cakes were equally good.',1,NULL),(7,'_____ in trying to solve this problem. It\'s clearly unsolvable.',1,NULL),(8,'Last year, when I last met her, she told me she _____ a letter every day for the last two months.',1,NULL),(9,'He _____ robbed as he was walking out of the bank.',1,NULL),(10,'_____ forced to do anything. He acted of his own free will.',1,NULL),(11,'It _____ the best idea to pay for those tickets by credit card. It was too risky.',1,NULL),(12,'They _____ in the basement for three months.',1,NULL),(13,'We\'ll never know what might have happened _____ the email earlier.',1,NULL),(14,'If success _____, we need to prepare ourselves for every possible scenario.',1,NULL),(15,'______ gifts to the judges.',1,NULL),(16,'Which animals do people think may be the cleverest?',3,1),(17,'What other beings are dolphins like in many ways?',3,1),(18,'What have scientists discovered about dolphins?',3,1),(19,'Why is a third language necessary if man wants to talk to dolphins?',3,1),(20,'In what way are dolphins friendly to man?',3,1),(21,' ',2,2);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_choice`
--

DROP TABLE IF EXISTS `question_choice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question_choice` (
  `question_id` int NOT NULL,
  `choice_id` int NOT NULL,
  `is_correct` tinyint DEFAULT NULL,
  PRIMARY KEY (`question_id`,`choice_id`),
  KEY `fk_questionChoice_choice_idx` (`choice_id`),
  CONSTRAINT `fk_questionChoice_choice` FOREIGN KEY (`choice_id`) REFERENCES `choice` (`id`),
  CONSTRAINT `fk_questionChoice_question` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_choice`
--

LOCK TABLES `question_choice` WRITE;
/*!40000 ALTER TABLE `question_choice` DISABLE KEYS */;
INSERT INTO `question_choice` VALUES (1,1,0),(1,2,1),(1,3,0),(1,4,0),(2,5,1),(2,6,0),(2,7,0),(2,8,0),(3,9,0),(3,10,0),(3,11,0),(3,12,1),(4,13,0),(4,14,0),(4,15,1),(4,16,0),(5,17,0),(5,18,1),(5,19,0),(5,20,0),(6,21,1),(6,22,0),(6,23,0),(6,24,0),(7,25,1),(7,26,0),(7,27,0),(7,28,0),(8,29,0),(8,30,0),(8,31,1),(8,32,0),(9,33,0),(9,34,0),(9,35,1),(9,36,0),(10,37,1),(10,38,0),(10,39,0),(10,40,0),(11,41,1),(11,42,0),(11,43,0),(11,44,0),(12,45,0),(12,46,0),(12,47,1),(12,48,0),(13,49,0),(13,50,1),(13,51,0),(13,52,0),(14,53,1),(14,54,0),(14,55,0),(14,56,0),(15,57,0),(15,58,1),(15,59,0),(15,60,0);
/*!40000 ALTER TABLE `question_choice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `result` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date_practice` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `point` double DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_result_category_idx` (`category_id`),
  CONSTRAINT `fk_result_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (1,'15/12/2020',10,1,1),(2,'15/12/2020',10,1,NULL),(3,'0.000618811881188118',0,1,NULL),(4,'15/12/2020',0,1,NULL),(5,'2020/12/15 23:40:13',0,1,NULL),(6,'2020/12/15 23:40:51',0,1,NULL),(7,'2020/12/15 23:57:09',0,2,NULL),(8,'2020/12/16 01:45:38',10,1,NULL),(9,'2020/12/16 01:45:38',10,2,NULL),(10,'2020/12/16 01:53:16',0,3,NULL),(11,'2020/12/16 01:56:51',10,3,NULL);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-16  2:16:32
