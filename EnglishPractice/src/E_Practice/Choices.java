/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package E_Practice;

import java.util.List;
import java.util.Arrays;

/**
 *
 * @author USER
 */
public class Choices {
    private static int dem = 0;
    private int id = dem++;
    private List<String> content;
    
    public Choices(List<String> content){
        this.content = content;
    }

//    @Override
//    public boolean equals(Object o) {
//        MultipleQuestion mq = (MultipleQuestion) o;
//        
//        return this.content.equals(c.content);
////        return this.content.get(i)
//    }

    /**
     * @return the content
     */
    public List<String> getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(List<String> content) {
        this.content = content;
    }

    /**
     * @return the dem
     */
    public static int getDem() {
        return dem;
    }

    /**
     * @param aDem the dem to set
     */
    public static void setDem(int aDem) {
        dem = aDem;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    
}
