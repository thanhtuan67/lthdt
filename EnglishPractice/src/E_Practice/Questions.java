/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package E_Practice;

import java.util.ArrayList;

/**
 *
 * @author USER
 */
public class Questions {
    private static int dem = 0;
    private int id = ++dem;
    private String content;
    private QuestionList ql;

    public Questions(String content) {
        this.content = content;
    }
    
//    public abstract boolean checkAnswer (ArrayList<Choices> answers);

    @Override
    public String toString() {
        String m = String.format("%s\n", this.getContent());
        return m;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    
}
