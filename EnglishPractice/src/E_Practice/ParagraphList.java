/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package E_Practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Dell
 */
public class ParagraphList {

    private ArrayList<QuestionParagraph> list = new ArrayList<>();
    private List<String> choiceName = Arrays.asList("A", "B", "C", "D");
    private ArrayList<String> cName = new ArrayList<>();
    private double point;

    public void addParagraph(QuestionParagraph q) {
        this.getList().add(q);
    }

    public void paragraphPratice(Scanner scan) {
        QuestionParagraph para = this.getList().get(0);
        System.out.println(para.getParagraph());
        Conversation c = (Conversation) para;
        int check = 0;
        
        this.cName.addAll(this.choiceName);
        for (int i = 0; i < c.getQuestion().size(); i++) {
            System.out.printf("\n%d. %s", i+1, c.getQuestion().get(i));
            for (int j = 0; j < 4; j++) {
                System.out.printf("%s. %s\n", this.cName.get(j), c.getChoice().get(i).getContent().get(j));
            }
            System.out.print("Enter your answer: ");
            String a = scan.next();
            if (a.toUpperCase().equals(c.getCorrect().get(i))) {
                System.out.println("CORRECT!!!");
                check++;
            } else {
                System.out.println("IT'S WRONG!!!");
            }
        }
        double score = this.scoreCalc(c.getQuestion().size(), check);
        this.point = score;
        System.out.println("\nCorrect answers: " + check);
        System.out.printf("Total score: %.1f\n", this.point);
        
    }
    
    public double scoreCalc(int num, int check) {
        double standard = 10 * 1.0 / num;
        return check * standard;
    }

    /**
     * @return the list
     */
    public ArrayList<QuestionParagraph> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(ArrayList<QuestionParagraph> list) {
        this.list = list;
    }

    /**
     * @return the point
     */
    public double getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(double point) {
        this.point = point;
    }
}
