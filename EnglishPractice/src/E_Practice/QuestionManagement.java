/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package E_Practice;

import englishpractice.EnglishPractice;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dell
 */
public class QuestionManagement {

    String url = "jdbc:mysql://localhost:3306/e-question";
    String username = "root";
    String password = "12345678";
    private QuestionList questionList = new QuestionList();
    private ParagraphList paraList = new ParagraphList();

    public int countMultipleQ() {
        int id = 0;
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            Statement stm = conn.createStatement();
            ResultSet countQuest = stm.executeQuery("select count(id) as id from question where category_id = 1;");
            while (countQuest.next()) {
                id = countQuest.getInt("id");
            }

        } catch (SQLException ex) {
            Logger.getLogger(EnglishPractice.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Connect failed!!!");
        }
        return id;
    }

    public void AddQuestionFromDB() {
        ArrayList<String> question = new ArrayList<>();

        try {
            List<String> lc1;
            List<String> lc2;
            List<String> lc3;
            List<String> lc4;
            List<String> lc5;
            List<String> lc6;
            List<String> lc7;
            List<String> lc8;
            List<String> lc9;
            List<String> lc10;
            List<String> lc11;
            List<String> lc12;
            List<String> lc13;
            List<String> lc14;
            List<String> lc15;

            try (
                    Connection conn = DriverManager.getConnection(url, username, password);
                    Statement stm = conn.createStatement()) {

                int cateId = 1;
                ResultSet getQuestion = stm.executeQuery("select *\n"
                        + "from question\n"
                        + "where category_id = " + cateId);
                while (getQuestion.next()) {
                    String content = getQuestion.getString("question_content");
                    question.add(content);
                }

                lc1 = new ArrayList<>();
                int count = this.countMultipleQ();
                ResultSet getc1 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 1");
                while (getc1.next()) {
                    String content = getc1.getString("choice_content");
                    lc1.add(content);
                }

                lc2 = new ArrayList<>();
                ResultSet getc2 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 2;");
                while (getc2.next()) {
                    String content = getc2.getString("choice_content");
                    lc2.add(content);
                }
                lc3 = new ArrayList<>();
                ResultSet getc3 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 3;");
                while (getc3.next()) {
                    String content = getc3.getString("choice_content");
                    lc3.add(content);
                }
                lc4 = new ArrayList<>();
                ResultSet getc4 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 4;");
                while (getc4.next()) {
                    String content = getc4.getString("choice_content");
                    lc4.add(content);
                }
                lc5 = new ArrayList<>();
                ResultSet getc5 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 5;");
                while (getc5.next()) {
                    String content = getc5.getString("choice_content");
                    lc5.add(content);
                }
                lc6 = new ArrayList<>();
                ResultSet getc6 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 6;");
                while (getc6.next()) {
                    String content = getc6.getString("choice_content");
                    lc6.add(content);
                }
                lc7 = new ArrayList<>();
                ResultSet getc7 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 7;");
                while (getc7.next()) {
                    String content = getc7.getString("choice_content");
                    lc7.add(content);
                }
                lc8 = new ArrayList<>();
                ResultSet getc8 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 8;");
                while (getc8.next()) {
                    String content = getc8.getString("choice_content");
                    lc8.add(content);
                }
                lc9 = new ArrayList<>();
                ResultSet getc9 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 9;");
                while (getc9.next()) {
                    String content = getc9.getString("choice_content");
                    lc9.add(content);
                }
                lc10 = new ArrayList<>();
                ResultSet getc10 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 10;");
                while (getc10.next()) {
                    String content = getc10.getString("choice_content");
                    lc10.add(content);
                }
                lc11 = new ArrayList<>();
                ResultSet getc11 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 11;");
                while (getc11.next()) {
                    String content = getc11.getString("choice_content");
                    lc11.add(content);
                }
                lc12 = new ArrayList<>();
                ResultSet getc12 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 12;");
                while (getc12.next()) {
                    String content = getc12.getString("choice_content");
                    lc12.add(content);
                }
                lc13 = new ArrayList<>();
                ResultSet getc13 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 13;");
                while (getc13.next()) {
                    String content = getc13.getString("choice_content");
                    lc13.add(content);
                }
                lc14 = new ArrayList<>();
                ResultSet getc14 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 14;");
                while (getc14.next()) {
                    String content = getc14.getString("choice_content");
                    lc14.add(content);
                }
                lc15 = new ArrayList<>();
                ResultSet getc15 = stm.executeQuery("select qc.*, c.choice_content\n"
                        + "from question_choice qc inner join choice c "
                        + "on qc.choice_id = c.id\n"
                        + "where qc.question_id = 15;");
                while (getc15.next()) {
                    String content = getc15.getString("choice_content");
                    lc15.add(content);
                }
            }

            List<String> correctChoice = Arrays.asList("B", "A", "D", "C",
                    "B", "A", "A", "C",
                    "C", "A", "A", "C",
                    "A", "A", "B");

            Choices c1 = new Choices(lc1);
            Choices c2 = new Choices(lc2);
            Choices c3 = new Choices(lc3);
            Choices c4 = new Choices(lc4);
            Choices c5 = new Choices(lc5);
            Choices c6 = new Choices(lc6);
            Choices c7 = new Choices(lc7);
            Choices c8 = new Choices(lc8);
            Choices c9 = new Choices(lc9);
            Choices c10 = new Choices(lc10);
            Choices c11 = new Choices(lc11);
            Choices c12 = new Choices(lc12);
            Choices c13 = new Choices(lc13);
            Choices c14 = new Choices(lc14);
            Choices c15 = new Choices(lc15);

// ADD CHOICES
            ArrayList<Choices> lc = new ArrayList<>();
            lc.add(c1);
            lc.add(c2);
            lc.add(c3);
            lc.add(c4);
            lc.add(c5);
            lc.add(c6);
            lc.add(c7);
            lc.add(c8);
            lc.add(c9);
            lc.add(c10);
            lc.add(c11);
            lc.add(c12);
            lc.add(c13);
            lc.add(c14);
            lc.add(c15);

// ADD MULTIPLE QUESTION
            for (int i = 0; i < question.size(); i++) {
                Questions mcq = new MultipleQuestion(question.get(i), lc.get(i), correctChoice.get(i));
                questionList.Add_Question(mcq);
            }

        } catch (SQLException ex) {
            Logger.getLogger(EnglishPractice.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Connect failed!!!");
        }
    }

    public void AddConversationQuestion() throws SQLException {
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement stm = conn.createStatement();

        ArrayList<String> questions = new ArrayList<>();
        String para = "";

        ResultSet getParagraph = stm.executeQuery("select * from paragraph "
                + "where id = 1;");
        while (getParagraph.next()) {
            String content = getParagraph.getString("para_content");
            para = content;
        }

        ResultSet getQuestion = stm.executeQuery("select * from question where category_id = 3 && para_id = 1;");
        while (getQuestion.next()) {
            String content = getQuestion.getString("question_content");
            questions.add(content);
        }

        List<String> lc1 = new ArrayList<>();
        ResultSet getc1 = stm.executeQuery("SELECT c.choice_content\n"
                + "FROM choice c\n"
                + "WHERE c.question_id = 16;");
        while (getc1.next()) {
            String content = getc1.getString("choice_content");
            lc1.add(content);
        }

        List<String> lc2 = new ArrayList<>();
        ResultSet getc2 = stm.executeQuery("SELECT c.choice_content\n"
                + "FROM choice c\n"
                + "WHERE c.question_id = 17;");
        while (getc2.next()) {
            String content = getc2.getString("choice_content");
            lc2.add(content);
        }

        List<String> lc3 = new ArrayList<>();
        ResultSet getc3 = stm.executeQuery("SELECT c.choice_content\n"
                + "FROM choice c\n"
                + "WHERE c.question_id = 18;");
        while (getc3.next()) {
            String content = getc3.getString("choice_content");
            lc3.add(content);
        }

        List<String> lc4 = new ArrayList<>();
        ResultSet getc4 = stm.executeQuery("SELECT c.choice_content\n"
                + "FROM choice c\n"
                + "WHERE c.question_id = 19;");
        while (getc4.next()) {
            String content = getc4.getString("choice_content");
            lc4.add(content);
        }

        List<String> lc5 = new ArrayList<>();
        ResultSet getc5 = stm.executeQuery("SELECT c.choice_content\n"
                + "FROM choice c\n"
                + "WHERE c.question_id = 20;");
        while (getc5.next()) {
            String content = getc5.getString("choice_content");
            lc5.add(content);
        }

        Choices c1 = new Choices(lc1);
        Choices c2 = new Choices(lc2);
        Choices c3 = new Choices(lc3);
        Choices c4 = new Choices(lc4);
        Choices c5 = new Choices(lc5);

        ArrayList<Choices> arrayChoice1 = new ArrayList<>();
        arrayChoice1.add(c1);
        arrayChoice1.add(c2);
        arrayChoice1.add(c3);
        arrayChoice1.add(c4);
        arrayChoice1.add(c5);

        List<String> correctChoice = Arrays.asList("A", "B", "C", "B", "B");

        ArrayList<Questions> arrayQuestion = new ArrayList<>();
        for (int i = 0; i < questions.size(); i++) {
            Questions q1 = new Questions(questions.get(i));
            arrayQuestion.add(q1);
        }

        

        QuestionParagraph question = new Conversation(para, arrayQuestion, arrayChoice1, correctChoice);
        paraList.addParagraph(question);

//        System.out.println(questions.size());
//
        Scanner scan = new Scanner(System.in);
        paraList.paragraphPratice(scan);
    }

    public void AddIncomplete() throws SQLException {
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement stm = conn.createStatement();

        ArrayList<String> questions = new ArrayList<>();
        List<String> lq1 = Arrays.asList("", "", "", "", "");
        questions.addAll(lq1);

        String para = "";

        ResultSet getParagraph = stm.executeQuery("select * from paragraph "
                + "where id = 2;");
        while (getParagraph.next()) {
            String content = getParagraph.getString("para_content");
            para = content;
        }

        List<String> lc1 = Arrays.asList("to seeing", "to see", "seeing", "see");
        List<String> lc2 = Arrays.asList("to come", "to coming", "come", "came");
        List<String> lc3 = Arrays.asList("when", "what", "where", "which");
        List<String> lc4 = Arrays.asList("to say", "to tell", "to speak", "to chat");
        List<String> lc5 = Arrays.asList("stay", "to stay", "staying", "stayed");

        Choices c1 = new Choices(lc1);
        Choices c2 = new Choices(lc2);
        Choices c3 = new Choices(lc3);
        Choices c4 = new Choices(lc4);
        Choices c5 = new Choices(lc5);

        ArrayList<Choices> arrayChoice1 = new ArrayList<>();
        arrayChoice1.add(c1);
        arrayChoice1.add(c2);
        arrayChoice1.add(c3);
        arrayChoice1.add(c4);
        arrayChoice1.add(c5);

        List<String> correctChoice = Arrays.asList("A", "C", "C", "C", "A");

        ArrayList<Questions> arrayQuestion = new ArrayList<>();
        for (int i = 0; i < questions.size(); i++) {
            Questions q1 = new Questions(questions.get(i));
            arrayQuestion.add(q1);
        }

        ParagraphList p = new ParagraphList();

        QuestionParagraph question = new Incomplete(para, arrayQuestion, arrayChoice1, correctChoice);
        paraList.addParagraph(question);

//        System.out.println(questions.size());
//
        Scanner scan = new Scanner(System.in);
        paraList.paragraphPratice(scan);
        System.out.println("========= INCOMPLETE QUESTION PRACTICE =========\n");
    }

    /**
     * @return the questionList
     */
    public QuestionList getQl() {
        return questionList;
    }

    /**
     * @param ql
     */
    public void setQl(QuestionList ql) {
        this.questionList = ql;
    }

    /**
     * @return the paraList
     */
    public ParagraphList getParaList() {
        return paraList;
    }

    /**
     * @param paraList the paraList to set
     */
    public void setParaList(ParagraphList paraList) {
        this.paraList = paraList;
    }
}
