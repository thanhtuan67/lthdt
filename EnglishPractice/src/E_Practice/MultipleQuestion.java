/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package E_Practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author USER
 */
public class MultipleQuestion extends Questions {

    private List<String> choiceName = Arrays.asList("A", "B", "C", "D");
    private ArrayList<String> cName = new ArrayList<>();
    private Choices choice;
//    private Choices correct; -- đổi thành kiểu string
    private String correct;

    public MultipleQuestion(String content, Choices c, String correct) {
        super(content);

        this.choice = c;
        this.correct = correct;
    }

    @Override
    public String toString() {

        this.cName.addAll(this.choiceName);
        String m = super.toString();

//        for (Choices c : this.choice) {
//            for (int i = 0; i < this.choice.size(); i++) {
//            m += String.format("%s. %s \n", this.cName.get(c.getId()), c.getContent());
        for (int i = 0; i < 4; i++) {
            m += String.format("%s. %s \n", this.cName.get(i), this.choice.getContent().get(i));
        }
//        }

        return m;
    }
    

//    @Override
//    public boolean checkAnswer(ArrayList<Choices> answers) {
//        if (answers.size() == 1)
//            return answers.get(0).equals(this.correct);
//        return false;
//    }
    /**
     * @return the choice
     */
    public Choices getChoice() {
        return choice;
    }

    /**
     * @param choice the choice to set
     */
    public void setChoice(Choices choice) {
        this.choice = choice;
    }

    /**
     * @return the correct
     */
    public String getCorrect() {
        return correct;
    }

    /**
     * @param correct the correct to set
     */
    public void setCorrect(String correct) {
        this.correct = correct;
    }

    /**
     * @return the choiceName
     */
    public List<String> getChoiceName() {
        return choiceName;
    }

    /**
     * @param choiceName the choiceName to set
     */
    public void setChoiceName(List<String> choiceName) {
        this.choiceName = choiceName;
    }

    /**
     * @return the cName
     */
    public ArrayList<String> getcName() {
        return cName;
    }

    /**
     * @param cName the cName to set
     */
    public void setcName(ArrayList<String> cName) {
        this.cName = cName;
    }

}
