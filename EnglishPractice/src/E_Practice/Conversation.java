/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package E_Practice;

import java.awt.Choice;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Dell
 */
public class Conversation extends QuestionParagraph {

    private List<String> choiceName = Arrays.asList("A", "B", "C", "D");
    private ArrayList<String> cName = new ArrayList<>();
    private ArrayList<Questions> question;
    private ArrayList<Choices> choice;
    private List<String> correct;

    public Conversation(String paragraph,
            ArrayList<Questions> question,
            ArrayList<Choices> choice, List<String> correct) {
        super(paragraph);
        this.question = question;
        this.choice = choice;
        this.correct = correct;
    }

//    @Override
//    public String toString() {
//        String m = super.toString();
//        this.cName.addAll(this.choiceName);
//
//        for (int i = 0; i < this.getQuestion().size(); i++) {
//            Choices c = new Choices(this.choice.get(i).getContent());
//            for (int j = 0; j < 4; j++) {
//                m += String.format("%s. %s\n", this.cName.get(j), c.getContent().get(j));
//            }
//        }
//        return m;
//    }

    /**
     * @return the choice
     */
    public ArrayList<Choices> getChoice() {
        return choice;
    }

    /**
     * @param choice the choice to set
     */
    public void setChoice(ArrayList<Choices> choice) {
        this.choice = choice;
    }

    /**
     * @return the correct
     */
    public List<String> getCorrect() {
        return correct;
    }

    /**
     * @param correct the correct to set
     */
    public void setCorrect(List<String> correct) {
        this.correct = correct;
    }

    /**
     * @return the question
     */
    public ArrayList<Questions> getQuestion() {
        return question;
    }

    /**
     * @param question the question to set
     */
    public void setQuestion(ArrayList<Questions> question) {
        this.question = question;
    }

}
