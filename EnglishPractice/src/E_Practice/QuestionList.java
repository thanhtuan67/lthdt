/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package E_Practice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author USER
 */
public class QuestionList {
    protected double point;
    private ArrayList<Questions> ds = new ArrayList<>();

    public void Add_Question(Questions q) {
        this.getDs().add(q);
    }

    public void Remove_Question(Questions q) {
        this.getDs().remove(q);
    }

    public QuestionList Search(Scanner kw) {
        QuestionList ql = new QuestionList();
        System.out.print("Nhập câu hỏi muốn xóa: ");
        int k = kw.nextInt();
        for (Questions q : this.getDs()) {
            if (q.getContent().equals(ql.getDs().get(k))) {
                ql.Remove_Question(q);
                System.out.println("Xóa thành công\n");
            } else {
                System.out.println("Xóa thất bại\n");
            }
        }
        return ql;
    }

    public void mupltiplePractice(Scanner scan, int num) {
        Collections.shuffle(this.getDs());
        int check = 0;

        for (int i = 0; i < num; i++) {
            Questions q = this.getDs().get(i);
            if (q instanceof MultipleQuestion) {
                MultipleQuestion ql = (MultipleQuestion) q;
                int id = i + 1;
                System.out.println("\n" + id + ". " + q);
                System.out.print("Lựa chọn của bạn: ");
                String c = scan.next();
//                Choices c1 = ql.getChoice().getContent().get(c);
//                Choices c1 = ql.getChoice();
                if (c.toUpperCase().equals(ql.getCorrect())) {
                    System.out.println("CORRECT!!!");
                    check++;
                } else {
                    System.out.println("IT'S WRONG");
                }
            }
        }
        this.point = this.scoreCalc(num, check);
        System.out.println("Correct answers: " + check);
        System.out.printf("Total score: %.1f\n", this.point);
        
    }
    
     
    public double scoreCalc(int num, int check) {
        double standard = 10 * 1.0 / num;
        return check * standard;
    }

    /**
     * @return the ds
     */
    public ArrayList<Questions> getDs() {
        return ds;
    }

    /**
     * @param ds the ds to set
     */
    public void setDs(ArrayList<Questions> ds) {
        this.ds = ds;
    }

    /**
     * @return the point
     */
    public double getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(double point) {
        this.point = point;
    }
}
