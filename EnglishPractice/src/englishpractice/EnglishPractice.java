/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package englishpractice;

import E_Practice.Choices;
import E_Practice.Conversation;
import E_Practice.ParagraphList;
import E_Practice.QuestionList;
import E_Practice.QuestionManagement;
import E_Practice.QuestionParagraph;
import E_Practice.Questions;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Dell
 */
public class EnglishPractice {

    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws SQLException {

        test();

//        System.out.println(date);
//=====================CONVERSATION PRACTICE=========================//
//        qm.AddConversationQuestion();
//=====================INCOMPLETE PRACTICE=========================//
//        qm.AddIncomplete();
//=====================MULTIPLE CHOICE PRACTICE=========================//
//        Scanner scan = new Scanner(System.in);
//        qm.AddQuestionFromDB();
//        qm.getQl().mupltiplePractice(scan, 3);
    }

    public static void test() throws SQLException {
        QuestionManagement qm = new QuestionManagement();
//        CONNECTION
        String url = "jdbc:mysql://localhost:3306/e-question";
        String username = "root";
        String password = "12345678";
        Connection conn = DriverManager.getConnection(url, username, password);
        Statement stm = conn.createStatement();
//        GET CURRENT DATE
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String date = dtf.format(now);

        int cate;
        boolean exit = false;
        Scanner scan = new Scanner(System.in);

        while (!exit) {

            System.out.println("\n=============== ENGLISH PRACTICE ===============\n");
            System.out.println("1. Multiple choice");
            System.out.println("2. Incomplete");
            System.out.println("3. Conversation");
            System.out.println("4. Exit\n");
            System.out.print("Enter the category question you want to practice: ");
            cate = scan.nextInt();

            switch (cate) {
                case 1:

                    int quantity;

                    System.out.println("\n========= MULTIPLE CHOICE QUESTION =========\n");
                    qm.AddQuestionFromDB();
                    System.out.print("Enter number of questions: ");
                    quantity = scan.nextInt();
                    qm.getQl().mupltiplePractice(scan, quantity);
                    stm.executeUpdate(
                            "INSERT INTO "
                            + "result(`date_practice`, `point`, `category_id`) " // Chỗ này m thêm user id vào nữa là ok
                            + "VALUE ('" + date + "' ," + qm.getQl().getPoint() + "," + 1 + ")");
                    break;
                case 2:
                    System.out.println("\n========= INCOMPLETE QUESTION =========\n");
                    qm.AddIncomplete();
                    stm.executeUpdate(
                            "INSERT INTO "
                            + "result(`date_practice`, `point`, `category_id`) " // Chỗ này m thêm user id vào nữa là ok
                            + "VALUE ('" + date + "' ," + qm.getParaList().getPoint() + "," + 2 + ")");
                    break;
                case 3:
                    System.out.println("\n========= CONVERSATION QUESTION =========\n");
                    qm.AddConversationQuestion();
                    stm.executeUpdate(
                            "INSERT INTO "
                            + "result(`date_practice`, `point`, `category_id`) " // Chỗ này m thêm user id vào nữa là ok
                            + "VALUE ('" + date + "' ," + qm.getParaList().getPoint() + "," + 3 + ")");
                    break;
                case 4:
                    exit = true; //for exit
                    break;

                default:
                    System.out.println("Something went wrong. Please check your input again!!!");
            }
        }
    }
}
