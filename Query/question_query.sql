INSERT INTO category(id, category_name) VALUE(1, 'Multiple Choice');
INSERT INTO category(id, category_name) VALUE(2, 'Incomplete');
INSERT INTO category(id, category_name) VALUE(3, 'Conversation');

INSERT INTO `question_bank`.`question_choice` (`question_id`, `choice_id`) VALUES (1, 1);
INSERT INTO `question_bank`.`question_choice` (`question_id`, `choice_id`) VALUES (1, 2);
INSERT INTO `question_bank`.`question_choice` (`question_id`, `choice_id`) VALUES (1, 3);
INSERT INTO `question_bank`.`question_choice` (`question_id`, `choice_id`) VALUES (1, 4);
INSERT INTO `question_bank`.`question_choice` (`question_id`, `choice_id`) VALUES (1, 1);


CREATE TABLE `question_choice` (
    `question_id` INT NOT NULL,
    `choice_id` INT NOT NULL,
    `is_correct` BOOLEAN NOT NULL,
    PRIMARY KEY (`question_id`, `choice_id`),
    CONSTRAINT `constr_questionChoice_question_fk`
        FOREIGN KEY `question_fk` (`question_id`) REFERENCES `question` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `constr_questionChoice_choice_fk`
        FOREIGN KEY `choice_fk` (`choice_id`) REFERENCES `choice` (`id`)
        ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_general_ci