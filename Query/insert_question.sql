INSERT INTO `e-question`.`question` (`question_content`) VALUES ('I\'m very happy _____ in India. I really miss being there.');
INSERT INTO `e-question`.`question` (`question_content`) VALUES ('They didn\'t reach an agreement ______ their differences.');
INSERT INTO `e-question`.`question` (`question_content`) VALUES ('I wish I _____ those words. But now it\'s too late.');
INSERT INTO `e-question`.`question` (`question_content`) VALUES ('The woman, who has been missing for 10 days, is believed _____.');
INSERT INTO `e-question`.`question` (`question_content`) VALUES ('She was working on her computer with her baby next to _____.');



INSERT INTO `e-question`.`choice` (`id`, `choice_content`, `question_id`) VALUES (1, 'to live', '1');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('to have lived', '1');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('to be lived', '1');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('to be living', '1');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('on account of', '2');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('due', '2');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('because', '2');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('owing', '2');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('not having said', '3');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('have never said', '3');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('never said', '3');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('had never said', '3');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('to be abducted', '4');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('to be abducting', '4');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('to have been abducted', '4');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('to have been abducting', '4');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('herself', '5');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('her', '5');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('her own', '5');
INSERT INTO `e-question`.`choice` (`choice_content`, `question_id`) VALUES ('hers', '5');
-- 